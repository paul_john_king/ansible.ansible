<!--
The Bourne shell script `readme.sh` at the root of the project's directory
tree generates this document from the help message of the script `play`.  In
order to change this document, edit the function `_help` of the script
`play`, set the current working directory to the root of the project's
directory tree, and call `readme.sh`.
-->

Summary
-------

This Bourne-shell script runs specified Ansible playbooks on all machines with
a domain in a specified list of domains.

Usage
-----

The Ansible inventory _may_ assign a string to the `domain` variable for a
machine.  Each role and task in each Ansible playbook _should_ contain a
`when` clause that includes the condition

    domain is defined and domain in domains

The call

    play <playbook_1> ... <playbook_l> @ <domain_1> ... <domain_m> : <option_1> ... <option_n>

in the root of the Ansible directory tree executes the Ansible playbooks

    playbooks/<playbook_1>/main.yml
    ...
    playbooks/<playbook_l>/main.yml

on each machine in the Ansible inventory that

    has a value for the `domain` variable, and

    the value is one of <domain_1>, ..., or <domain_m>

with the `ansible-playbook` options

    <option_1> ... <option_n>

As a convenience, the call also

sets the value of the `pwd` Ansible variable to the call's current
working directory, and 

sets all of the variable assignments in the file `global_vars.yml` if the
file exists.

`readme.sh`
-------------

The Bourne shell script `readme.sh` at the root of the project's directory
tree generates this document from the help message of the script `play`.  In
order to change this document, edit the function `_help` of the script
`play`, set the current working directory to the root of the project's
directory tree, and call `readme.sh`.
